using System.ComponentModel.DataAnnotations.Schema;

namespace BetmaxReferralService.Models;

public class Promocode
{
    public int Id {get; set;}
    public int ReferralId {get; set;}
    public string Code {get; set;}
    public int Discount {get; set;}
    public int ReferralIncome {get; set;}
    public int TrialLengthIncrease {get; set;}
    public int MaxApplies { get; set; }
}