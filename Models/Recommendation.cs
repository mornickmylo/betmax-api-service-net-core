﻿using System;
using System.Collections.Generic;

namespace BetmaxReferralService.Models
{
    public class Recommendation
    {
        public int Id { get; set; }
        public string? TitleRu { get; set; }
        public string? TitleEn { get; set; }
        public string? Background { get; set; }
        public string? HrefRu { get; set; }
        public string? HrefEn { get; set; }
    }
}
