using BetmaxReferralService.AppSettingsModels;
using BetmaxReferralService.Interfaces;
using BetmaxReferralService.Models;
using Microsoft.Extensions.Options;

namespace BetmaxReferralService.Services;

public class ProUserService : UserService<ProUser>
{
    public ProUserService(ICodesService codesService, CommonContext db, IUserContext<ProUser> userContext,
        IOptions<JwtSettings> jwtSettings) : base(codesService, db, userContext, jwtSettings)
    {
        
    }

    public override async Task<LoginConfirmResult<ProUser>> LoginConfirm(string login, string code)
    {
        var result = await base.LoginConfirm(login, code);
        if (result.CodesCorrect && result.UserFound)
        {
            var proData = this.db.ProData.Find(result.Data.Id);
            proData.NeedLogout = 0;
            this.db.ProData.Update(proData);
            await this.db.SaveChangesAsync();
        }

        return result;
    }
}