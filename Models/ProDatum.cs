﻿using System;
using System.Collections.Generic;

namespace BetmaxReferralService.Models
{
    public class ProDatum
    {
        public int UserId { get; set; }
        public ProUser User { get; set; }
        public ulong? TrialAvailable { get; set; }
        public DateTime? ActiveUntil { get; set; }
        public int? PhoneCheckFails { get; set; }
        public ulong? NeedLogout { get; set; }
    }
}
