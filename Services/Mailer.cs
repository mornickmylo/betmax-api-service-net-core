using BetmaxReferralService.Interfaces;
using BetmaxReferralService.AppSettingsModels;
using BetmaxReferralService.Models;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Options;
using MimeKit;

namespace BetmaxReferralService.Services;

public class Mailer : IMailer
{
    private readonly MailSettings mailSettings;
    private readonly IEmailRenderer emailRenderer;
    
    public Mailer(IOptions<MailSettings> mailSettings, IEmailRenderer emailRenderer)
    {
        this.mailSettings = mailSettings.Value;
        this.emailRenderer = emailRenderer;
    }

    public async Task Send<T>(T model) where T : EmailModelBase
    {
        var email = new MimeMessage();
        email.To.Add(MailboxAddress.Parse(model.To));
        email.Subject = model.Subject;
        var builder = new BodyBuilder
        {
            HtmlBody = await this.emailRenderer.Render(model),
        };
        email.Body = builder.ToMessageBody();
        email.Sender = MailboxAddress.Parse(this.mailSettings.DisplayName);
        email.From.Add(MailboxAddress.Parse(this.mailSettings.DisplayName));
        using var smtp = new SmtpClient();
        await smtp.ConnectAsync(this.mailSettings.Host, this.mailSettings.Port, MailKit.Security.SecureSocketOptions.SslOnConnect);
        await smtp.AuthenticateAsync(this.mailSettings.Mail, this.mailSettings.Password);
        await smtp.SendAsync(email);
        await smtp.DisconnectAsync(true);
    }
}