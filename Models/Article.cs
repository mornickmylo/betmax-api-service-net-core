﻿using System;
using System.Collections.Generic;

namespace BetmaxReferralService.Models
{
    public class Article
    {
        public int Id { get; set; }
        public string? TitleRu { get; set; }
        public string? TextRu { get; set; }
        public string? TitleEn { get; set; }
        public string? TextEn { get; set; }
        public string? Cpu { get; set; }
        public string? Background { get; set; }
        public DateOnly? Date { get; set; }
        public string? DescRu { get; set; }
        public string? DescEn { get; set; }
        public string? TagsRu { get; set; }
        public string? TagsEn { get; set; }
        public string? TitleTagRu { get; set; }
        public string? TitleTagEn { get; set; }
        public int? Priority { get; set; }
    }
}
