using BetmaxReferralService.Interfaces;
using BetmaxReferralService.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace BetmaxReferralService.Controllers;

[ApiController]
public abstract class UserController<T> : ControllerBase where T : class, IUser
{
    private readonly IUserService<T> userService;
    protected UserController(IUserService<T> userService)
    {
        this.userService = userService;
    }
    [HttpPost("send-code")]
    public async Task<IActionResult> SendCode(SendCodeRequest request)
    {
        var result = await this.userService.SendCode(request.type, request.value);
        if (result != null)
        {
            return Ok(result);
        }

        return StatusCode(500, $"Ошибка при отправке кода подтверждения на {request.value}. Обратитесь в поддержку");
    }

    [HttpPost("login")]
    public async Task<IActionResult> Login(LoginRequest request)
    {
        var result = await this.userService.SendLoginCode(request.login);
        if (result.code == null)
        {
            if (!result.UserFound)
            {
                return NotFound();
            }
            return StatusCode(500, $"Ошибка при отправке кода подтверждения на {request.login}. Обратитесь в поддержку");
        }

        return Ok(JsonConvert.SerializeObject(new
        {
            result.code,
            type = result.LoginType.ToString().ToLower()
        }));
    }

    [HttpPost("upsert")]
    public async Task<IActionResult> Upsert(UpsertRequest request, [FromHeader(Name = "X-Forwarded-For")] string clientIp)
    {
        UpsertResult<T> result = request.NewData != null 
            ? await this.userService.Edit(request.Data, request.NewData, clientIp) 
            : await this.userService.Register(request.Data, clientIp);
        if (!result.CodesCorrect)
        {
            return StatusCode(401);
        }
        if (result.ConflictField != null)
        {
            return Conflict($"{result.ConflictField} taken");
        }
        return Ok(JsonConvert.SerializeObject(new
        {
            data = result.Data,
            token = result.Token
        }));
    }

    [HttpPost("login-confirm")]
    public async Task<IActionResult> LoginConfirm(LoginConfirm request)
    {
        var result = await this.userService.LoginConfirm(request.Login, request.Code);
        if (!result.CodesCorrect)
        {
            return StatusCode(401);
        }
        if (!result.UserFound)
        {
            return StatusCode(404);
        }
        return Ok(JsonConvert.SerializeObject(new
        {
            data = result.Data,
            token = result.Token
        }));
    }
}