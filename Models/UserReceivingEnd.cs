namespace BetmaxReferralService.Models;

public enum UserReceivingEnd {
    Phone,
    Email
};