namespace BetmaxReferralService.Models;

public class EmailModelBase
{
    public string To { get; set; }
    public string Subject { get; set; }
}