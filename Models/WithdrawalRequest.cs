﻿using System;
using System.Collections.Generic;

namespace BetmaxReferralService.Models
{
    public class WithdrawalRequest
    {
        public int UserId { get; set; }
        public string Account { get; set; } = null!;
        public string Type { get; set; } = null!;
        public decimal Sum { get; set; }
        public int Id { get; set; }
        public sbyte Approved { get; set; }
        public DateTime Date { get; set; }
        public string? RejectReason { get; set; }
    }
}
