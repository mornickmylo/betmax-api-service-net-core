
using BetmaxReferralService.AppSettingsModels;
using BetmaxReferralService.Interfaces;
using BetmaxReferralService.Models;
using Microsoft.Extensions.Options;

namespace BetmaxReferralService.Services;

public class ReferralService : UserService<Referral>
{
    private readonly IMailer mailer;
    public ReferralService(ICodesService codesService, CommonContext db, IUserContext<Referral> userContext,
        IOptions<JwtSettings> jwtSettings, IMailer mailer) : base(codesService, db, userContext, jwtSettings)
    {
        this.mailer = mailer;
    }

    public override async Task<UpsertResult<Referral>> Register(UserShortWithCodes data, string clientIp)
    {
        var registerResult = await base.Register(data, clientIp);
        var registeredReferral = registerResult.Data;
        if (registeredReferral == null) return registerResult;
        registeredReferral.Link = this.CreateLink();
        this.userContext.Users.Update(registeredReferral);
        await this.userContext.SaveChangesAsync();
        await this.mailer.Send(new ReferralRegisterEmail
        {
            To = registeredReferral.Email,
            Subject = "Партнерская программа BetMAX",
            InstallPrice = this.db.InstallPrices.First().Price,
            ReferralLink = registeredReferral.Link
        });
        var promocode = new Promocode
        {
            ReferralId = registeredReferral.Id,
            Code = this.GenerateRandomStr(10).ToUpper(),
            Discount = 10,
            ReferralIncome = 20,
            TrialLengthIncrease = 0,
            MaxApplies = 0
        };
        this.db.Promocodes.Add(promocode);
        await this.db.SaveChangesAsync();
        return registerResult;
    }

    private string CreateLink()
    {
        var link = this.GenerateLink();
        while (this.userContext.Users.Any(r => r.Link == link))
        {
            link = this.GenerateLink();
        }

        return link;
    }

    private string GenerateLink()
    {
        return $"https://betmax.ru/ref/{this.GenerateRandomStr(6)}";
    }
    
    private string GenerateRandomStr(int length) {
        var randomStr = "";
        var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".ToCharArray();
        var rand = new Random();
        for (var i = 0; i < length; i++)
        {
            rand.NextInt64(0, characters.Length - 1);
            randomStr += characters[rand.NextInt64(0, characters.Length - 1)];
        }
        return randomStr;
    }
}