using System.Text;
using BetmaxReferralService;
using BetmaxReferralService.AppSettingsModels;
using BetmaxReferralService.Interfaces;
using BetmaxReferralService.Models;
using BetmaxReferralService.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using ProUser = BetmaxReferralService.Models.ProUser;

void AddDbContext<TService, TImplementation>(WebApplicationBuilder builder, string connectionString, ServerVersion version) where TImplementation : DbContext, TService
{
    builder.Services.AddDbContext<TService, TImplementation>(
        dbContextOptions => dbContextOptions
            .UseMySql(connectionString, version)
            // The following three options help with debugging, but should
            // be changed or removed for production.
            .LogTo(Console.WriteLine, LogLevel.Information)
            .EnableSensitiveDataLogging()
            .EnableDetailedErrors()
    );
}

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var mySqlSettingsSection = builder.Configuration.GetSection("MySqlSettings");
var mySqlSettings = mySqlSettingsSection.Get<MySqlSettings>();
var serverVersion = new MySqlServerVersion(new Version(mySqlSettings.Major, mySqlSettings.Minor, mySqlSettings.Build));
AddDbContext<IUserContext<ProUser>, ProUsersContext>(builder, mySqlSettings.ConnectionString, serverVersion);
AddDbContext<IUserContext<Referral>, ReferralsContext>(builder, mySqlSettings.ConnectionString, serverVersion);
AddDbContext<CommonContext, CommonContext>(builder, mySqlSettings.ConnectionString, serverVersion);

builder.Services.Configure<MailSettings>(builder.Configuration.GetSection("MailSettings"));
var jwtSettingsSection = builder.Configuration.GetSection("JwtSettings");
builder.Services.Configure<JwtSettings>(jwtSettingsSection);
builder.Services.AddMvcCore().AddRazorViewEngine();
builder.Services.AddTransient<IEmailRenderer, RazorEmailRenderer>();
builder.Services.AddTransient<IMailer, Mailer>();
builder.Services.AddHttpClient();
builder.Services.AddTransient<ICodesService, CodesService>();
builder.Services.AddTransient<IUserService<ProUser>, ProUserService>();
builder.Services.AddTransient<IUserService<Referral>, ReferralService>();

var jwtSettings = jwtSettingsSection.Get<JwtSettings>();
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.RequireHttpsMetadata = false;
        options.TokenValidationParameters = new TokenValidationParameters
        {
            // укзывает, будет ли валидироваться издатель при валидации токена
            ValidateIssuer = true,
            // строка, представляющая издателя
            ValidIssuer = jwtSettings.Issuer,
 
            // будет ли валидироваться потребитель токена
            ValidateAudience = true,
            // установка потребителя токена
            ValidAudience = jwtSettings.Audience,
            // будет ли валидироваться время существования
            ValidateLifetime = true,
 
            // установка ключа безопасности
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(jwtSettings.SecretKey)),
            // валидация ключа безопасности
            ValidateIssuerSigningKey = true,
        };
    });

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();