using System.ComponentModel.DataAnnotations.Schema;
using BetmaxReferralService.Interfaces;

namespace BetmaxReferralService.Models;

public class Referral : UserShort, IUser
{
    public int Id { get; set; }
    public DateTime Registered { get; set; }
    public bool Confirmed { get; set; }
    public string Link { get; set; }
    public float Balance { get; set; }
    public IEnumerable<Click> Clicks { get; set; }
}