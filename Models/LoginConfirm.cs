namespace BetmaxReferralService.Models;

public class LoginConfirm
{
    public string Code { get; set; }
    public string Login { get; set; }
}