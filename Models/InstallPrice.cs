using System.ComponentModel.DataAnnotations.Schema;

namespace BetmaxReferralService.Models;

public class InstallPrice
{
    public int Price { get; set; }
}