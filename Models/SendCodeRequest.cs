namespace BetmaxReferralService.Models;

public class SendCodeRequest
{
    public string type { get; set; }
    public string value { get; set; }
}