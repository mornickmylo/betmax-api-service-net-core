﻿using System;
using System.Collections.Generic;

namespace BetmaxReferralService.Models
{
    public class UsersToCheckPhone
    {
        public int Id { get; set; }
        public DateOnly? Date { get; set; }
        public string? UserIds { get; set; }
    }
}
