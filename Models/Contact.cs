﻿using System;
using System.Collections.Generic;

namespace BetmaxReferralService.Models
{
    public class Contact
    {
        public int Id { get; set; }
        public string? Email { get; set; }
        public string? Name { get; set; }
        public string? Skype { get; set; }
        public string? Wv { get; set; }
        public string? Tg { get; set; }
        public string? Socials { get; set; }
        public int? PollId { get; set; }
    }
}
