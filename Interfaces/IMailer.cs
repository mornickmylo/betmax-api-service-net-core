using BetmaxReferralService.Models;

namespace BetmaxReferralService.Interfaces;

public interface IMailer
{
    Task Send<T>(T model) where T : EmailModelBase;
}