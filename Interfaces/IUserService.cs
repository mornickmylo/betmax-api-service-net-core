using BetmaxReferralService.Models;

namespace BetmaxReferralService.Interfaces;

public interface IUserService<T> where T : class, IUser
{
    Task<string?> SendCode(string strType, string value);
    Task<LoginResult> SendLoginCode(string value);
    Task<UpsertResult<T>> Register(UserShortWithCodes data, string clientIp);
    Task<UpsertResult<T>> Edit(UserShortWithCodes data, UserShort newData, string clientIp);
    Task<LoginConfirmResult<T>> LoginConfirm(string login, string code);
}