namespace BetmaxReferralService.Models;

public class ProTrialEmail : EmailModelBase
{
    public string TrialLength { get; set; }
    public string Username { get; set; }
    public DateTime EndDate { get; set; }
}