using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace BetmaxReferralService.Models;

public class ConfirmationCode
{
    public string Type { get; set; }
    public string Address { get; set; }
    public string Code { get; set; }
}