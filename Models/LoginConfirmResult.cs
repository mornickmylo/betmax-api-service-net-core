using BetmaxReferralService.Interfaces;

namespace BetmaxReferralService.Models;

public class LoginConfirmResult<T> where T : class, IUser
{
    public T Data { get; set; }
    public string Token { get; set; }
    public bool CodesCorrect { get; set; }
    public bool UserFound { get; set; }
}