using BetmaxReferralService.Interfaces;
using BetmaxReferralService.Models;
using Microsoft.EntityFrameworkCore;

namespace BetmaxReferralService;

public class ReferralsContext : DbContext, IUserContext<Referral>
{
    public DbSet<Referral> Users { get; set; }
    
    public ReferralsContext(DbContextOptions<ReferralsContext> options) : base(options)
    {
        Database.EnsureCreated();
    }
    
    public Referral Add(UserShort user)
    {
        var addedEntity = this.Users.Add(new Referral
        {
            Phone = user.Phone,
            Email = user.Email,
            Confirmed = true,
            Registered = DateTime.Now,
            Name = user.Name
        });
        this.SaveChanges();
        return addedEntity.Entity;
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Referral>(entity =>
        {
            entity.ToTable("referals");

            entity.UseCollation("utf8mb4_unicode_ci");

            entity.Property(e => e.Id).HasColumnName("id");

            entity.Property(e => e.Balance)
                .HasPrecision(10)
                .HasColumnName("balance");

            entity.Property(e => e.Confirmed)
                .HasColumnType("bit(1)")
                .HasColumnName("confirmed");

            entity.Property(e => e.Email)
                .HasMaxLength(255)
                .HasColumnName("email");

            entity.Property(e => e.Link)
                .HasMaxLength(45)
                .HasColumnName("link");

            entity.Property(e => e.Name)
                .HasMaxLength(255)
                .HasColumnName("name")
                .UseCollation("utf8mb4_0900_ai_ci");

            entity.Property(e => e.Phone)
                .HasMaxLength(20)
                .HasColumnName("phone");

            entity.Property(e => e.Registered)
                .HasColumnType("datetime")
                .HasColumnName("registered");
        });
        
        modelBuilder.Entity<Click>(entity =>
        {
            entity.ToTable("clicks");

            entity.UseCollation("utf8mb4_unicode_ci");

            entity.Property(e => e.Date)
                .HasColumnType("datetime")
                .HasColumnName("date");

            entity.Property(e => e.UserId).HasColumnName("userId");

            entity.HasOne(c => c.User)
                .WithMany(r => r.Clicks)
                .HasForeignKey(c => c.UserId);
        });
    }
}