﻿using System;
using System.Collections.Generic;

namespace BetmaxReferralService.Models
{
    public class Install
    {
        public int UserId { get; set; }
        public DateTime? Date { get; set; }
        public string? NewUserId { get; set; }
        public ulong? Confirmed { get; set; }
        public string? Ip { get; set; }
    }
}
