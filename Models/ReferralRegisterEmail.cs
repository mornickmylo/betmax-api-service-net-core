namespace BetmaxReferralService.Models;

public class ReferralRegisterEmail : EmailModelBase
{
    public float InstallPrice { get; set; }
    public string ReferralLink { get; set; }
}