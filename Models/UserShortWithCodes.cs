namespace BetmaxReferralService.Models;

public class UserShortWithCodes : UserShort
{
    public Dictionary<string, string> Codes { get; set; }
}