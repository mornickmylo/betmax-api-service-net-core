using BetmaxReferralService.Interfaces;
using BetmaxReferralService.Models;
using Microsoft.AspNetCore.Mvc;

namespace BetmaxReferralService.Controllers;

public class ReferralController : UserController<Referral>
{
    private readonly IUserService<Referral> referralService;
    public ReferralController(IUserService<Referral> referralService) : base(referralService)
    {
        this.referralService = referralService;
    }
}