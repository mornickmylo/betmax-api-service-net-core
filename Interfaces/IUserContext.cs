using BetmaxReferralService.Models;
using Microsoft.EntityFrameworkCore;

namespace BetmaxReferralService.Interfaces;

public interface IUserContext<T> where T : class, IUser
{
    DbSet<T> Users { get; }
    T Add(UserShort user);
    int SaveChanges();
    Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
}