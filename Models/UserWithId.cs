namespace BetmaxReferralService.Models;

public class UserWithId
{
    public string Name { get; set; }
    public string Phone { get; set; }
    public string Email { get; set; }
}