﻿using System;
using System.Collections.Generic;

namespace BetmaxReferralService.Models
{
    public class ActionsCount
    {
        public string UserId { get; set; } = null!;
        public sbyte? Count { get; set; }
    }
}
