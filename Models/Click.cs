﻿using System;
using System.Collections.Generic;

namespace BetmaxReferralService.Models
{
    public class Click
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public DateTime? Date { get; set; }
        public Referral User { get; set; }
    }
}
