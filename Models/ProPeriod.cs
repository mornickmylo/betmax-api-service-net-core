﻿using System;
using System.Collections.Generic;

namespace BetmaxReferralService.Models
{
    public class ProPeriod
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public float? Price { get; set; }
        public sbyte? Discount { get; set; }
        public short? Length { get; set; }
        public ulong? IsTrial { get; set; }
    }
}
