﻿using System;
using System.Collections.Generic;

namespace BetmaxReferralService.Models
{
    public class ProPayment
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public float? Price { get; set; }
        public short? Length { get; set; }
        public DateTime? Date { get; set; }
        public int? UserId { get; set; }
        public DateTime? StartsFrom { get; set; }
        public string? IsTrial { get; set; }
        public int? PromocodeId { get; set; }
    }
}
