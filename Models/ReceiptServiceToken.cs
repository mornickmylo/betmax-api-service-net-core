﻿using System;
using System.Collections.Generic;

namespace BetmaxReferralService.Models
{
    public class ReceiptServiceToken
    {
        public string? Token { get; set; }
    }
}
