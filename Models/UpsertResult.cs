using BetmaxReferralService.Interfaces;

namespace BetmaxReferralService.Models;

public class UpsertResult<T> where T : class, IUser
{
    public T Data { get; set; }
    public string Token { get; set; }
    public string ConflictField { get; set; }
    public bool CodesCorrect { get; set; }
}