namespace BetmaxReferralService.Models;

public class LoginResult
{
    public bool UserFound { get; set; }
    public UserReceivingEnd LoginType { get; set; }
    public string code { get; set; }
}