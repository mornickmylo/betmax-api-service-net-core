using BetmaxReferralService.Interfaces;

namespace BetmaxReferralService.Models;

public class ProUser : UserShort, IUser
{
    public int Id { get; set; }
    public DateTime Registered { get; set; }
    public bool Confirmed { get; set; }
    public ProDatum ProDatum { get; set; } 
}