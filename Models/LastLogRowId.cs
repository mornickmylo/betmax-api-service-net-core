﻿using System;
using System.Collections.Generic;

namespace BetmaxReferralService.Models
{
    public class LastLogRowId
    {
        public string Id { get; set; } = null!;
    }
}
