namespace BetmaxReferralService.Models;

public class UpsertRequest
{
    public UserShortWithCodes Data { get; set; }
    public UserShort? NewData { get; set; }
}