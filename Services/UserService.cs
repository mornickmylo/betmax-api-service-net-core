using System.Security.Claims;
using BetmaxReferralService.AppSettingsModels;
using BetmaxReferralService.Interfaces;
using BetmaxReferralService.Models;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Text;

namespace BetmaxReferralService.Services;

public class UserService<T> : IUserService<T> where T : class, IUser, new()
{
    private readonly ICodesService codesService;
    protected readonly CommonContext db;
    protected readonly IUserContext<T> userContext;
    private readonly JwtSettings _jwtSettings;
    
    public UserService(ICodesService codesService, CommonContext db, IUserContext<T> userContext, IOptions<JwtSettings> jwtSettings)
    {
        this.codesService = codesService;
        this.db = db;
        this.userContext = userContext;
        this._jwtSettings = jwtSettings.Value;
    }
    
    

    public async Task<string?> SendCode(string strType, string value)
    {
        if (UserReceivingEnd.TryParse(strType.FirstCharToUpper(), out UserReceivingEnd type))
        {
            return await this.SendCode(type, strType, value);
        }

        return null;
    }
    public async Task<LoginResult> SendLoginCode(string value)
    {
        var user = this.userContext.Users.FirstOrDefault(u => u.Phone == value);
        var type = UserReceivingEnd.Phone;
        if (user == null)
        {
            type = UserReceivingEnd.Email;
            user = this.userContext.Users.FirstOrDefault(u => u.Email == value);
        }
        if (user == null)
        {
            return new LoginResult()
            {
                UserFound = false,
                LoginType = type,
                code = null
            };
        }
        return new LoginResult()
        {
            UserFound = true,
            LoginType = type,
            code = await this.SendCode(type, "unknown", value)
        };
    }

    private async Task<string?> SendCode(UserReceivingEnd type, string typeName, string value)
    {
        var senders = new Dictionary<UserReceivingEnd, Func<string, Task<string>>>
        {
            { UserReceivingEnd.Phone, (string address) => this.codesService.SendToPhone(address) },
            { UserReceivingEnd.Email, (string address) => this.codesService.SendToEmail(address, typeof(T)) }
        };
        var code = await senders[type](value);
        if (code != null)
        {
            var existingCode = this.db.ConfirmationCodes.Find(typeName, value);
            if (existingCode == null)
            {
                this.db.ConfirmationCodes.Add(new ConfirmationCode()
                {
                    Address = value,
                    Type = typeName,
                    Code = code
                });
            }
            else
            {
                existingCode.Code = code;
                this.db.ConfirmationCodes.Update(existingCode);
            }

            await this.db.SaveChangesAsync();
        }

        return code;
    }

    public virtual async Task<UpsertResult<T>> Register(UserShortWithCodes data, string clientIp)
    {
        if (this.CheckCodes(data.Phone, data.Email, data.Codes["phone"], data.Codes["email"]))
        {
            var conflictField = this.GetTakenUserData(data.Phone, data.Email);
            if (conflictField != null)
            {
                return new UpsertResult<T>
                {
                    CodesCorrect = true,
                    ConflictField = conflictField
                };
            }

            var newUser = this.userContext.Add(data);
            
            return new UpsertResult<T>
            {
                CodesCorrect = true,
                ConflictField = null,
                Data = newUser,
                Token = this.GetToken(newUser)
            };
        }

        return new UpsertResult<T>
        {
            CodesCorrect = false
        };
    }

    public async Task<UpsertResult<T>> Edit(UserShortWithCodes data, UserShort newData, string clientIp)
    {
        if (this.CheckCodes(newData.Phone, newData.Email, data.Codes["phone"], data.Codes["email"]))
        {
            var user = this.userContext.Users.FirstOrDefault(u => u.Phone == data.Phone);
            user.Name = newData.Name;
            user.Email = newData.Email;
            user.Phone = newData.Phone;
            this.userContext.Users.Update(user);
            await this.userContext.SaveChangesAsync();
            return new UpsertResult<T>
            {
                CodesCorrect = true,
                ConflictField = null,
                Data = user,
                Token = this.GetToken(user)
            };
        }
        return new UpsertResult<T>
        {
            CodesCorrect = false
        };
    }

    public virtual async Task<LoginConfirmResult<T>> LoginConfirm(string login, string code)
    {
        var existingCode = this.db.ConfirmationCodes.Find("unknown", login);
        if (existingCode == null || existingCode.Code != code)
        {
            return new LoginConfirmResult<T>()
            {
                CodesCorrect = false
            };
        }

        var user = this.userContext.Users.FirstOrDefault(u => u.Email == login || u.Phone == login);
        if (user == null)
        {
            return new LoginConfirmResult<T>()
            {
                CodesCorrect = true,
                UserFound = false
            };
        }

        return new LoginConfirmResult<T>()
        {
            CodesCorrect = true,
            UserFound = true,
            Data = user,
            Token = this.GetToken(user)
        };
    }

    private bool CheckCodes(string phone, string email, string phoneCode, string emailCode)
    {
        return this.CheckCode(email, "email", emailCode) && this.CheckCode(phone, "phone", phoneCode);
    }

    private bool CheckCode(string address, string type, string code)
    {
        return this.db.ConfirmationCodes.FirstOrDefault(c =>
            c.Address == address && c.Type == type && c.Code == code) != null;
    }

    private string GetTakenUserData(string phone, string email)
    {
        var existingUser = this.userContext.Users.FirstOrDefault(u => u.Phone == phone || u.Email == email);
        if (existingUser != null)
        {
            return existingUser.Email == email ? "email" : "phone";
        }

        return null;
    }

    private string GetToken(IUser user)
    {
        var identity = this.GetIdentity(user);
        // создаем JWT-токен
        var jwt = new JwtSecurityToken(
            issuer: this._jwtSettings.Issuer,
            audience: this._jwtSettings.Audience,
            notBefore: DateTime.UtcNow,
            claims: identity.Claims,
            expires: DateTime.UtcNow.Add(TimeSpan.FromDays(this._jwtSettings.Lifetime)),
            signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.ASCII.GetBytes(this._jwtSettings.SecretKey)), SecurityAlgorithms.HmacSha256));
        return new JwtSecurityTokenHandler().WriteToken(jwt);
    }
    
    private ClaimsIdentity GetIdentity(IUser user)
    {
        var claims = new List<Claim>
        {
            new Claim(ClaimsIdentity.DefaultNameClaimType, user.Name),
            new Claim(ClaimsIdentity.DefaultRoleClaimType, "user")
        };
        ClaimsIdentity claimsIdentity =
            new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
        return claimsIdentity;
    }
}