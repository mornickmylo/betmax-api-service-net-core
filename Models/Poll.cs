﻿using System;
using System.Collections.Generic;

namespace BetmaxReferralService.Models
{
    public class Poll
    {
        public int Id { get; set; }
        public string? BkCount { get; set; }
        public string? Device { get; set; }
        public string? Frequency { get; set; }
        public string? InstrumentList { get; set; }
        public string? Instruments { get; set; }
        public string? Live { get; set; }
        public string? Sports { get; set; }
        public string? Prematch { get; set; }
    }
}
