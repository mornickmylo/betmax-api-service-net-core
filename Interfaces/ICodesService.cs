using BetmaxReferralService.Models;

namespace BetmaxReferralService.Interfaces;

public interface ICodesService
{
    Task<string> SendToPhone(string phone);
    Task<string> SendToEmail(string email, Type userType);
}