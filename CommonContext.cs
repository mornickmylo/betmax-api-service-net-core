using BetmaxReferralService.Models;
using Microsoft.EntityFrameworkCore;

namespace BetmaxReferralService;

public class CommonContext : DbContext
{
    public DbSet<ConfirmationCode> ConfirmationCodes { get; set; }
    public DbSet<InstallPrice> InstallPrices { get; set; }
    public DbSet<Promocode> Promocodes { get; set; }
    public DbSet<ActionsCount> ActionsCounts { get; set; } = null!;
    public DbSet<Article> Articles { get; set; } = null!;
    public DbSet<Click> Clicks { get; set; } = null!;
    public DbSet<Contact> Contacts { get; set; } = null!;
    public DbSet<Install> Installs { get; set; } = null!;
    public DbSet<LastLogRowId> LastLogRowIds { get; set; } = null!;
    public DbSet<Poll> Polls { get; set; } = null!;
    public DbSet<ProDatum> ProData { get; set; } = null!;
    public DbSet<ProPayment> ProPayments { get; set; } = null!;
    public DbSet<ProPeriod> ProPeriods { get; set; } = null!;
    public DbSet<ReceiptServiceToken> ReceiptServiceTokens { get; set; } = null!;
    public DbSet<Recommendation> Recommendations { get; set; } = null!;
    public DbSet<Token> Tokens { get; set; } = null!;
    public DbSet<UsersToCheckPhone> UsersToCheckPhones { get; set; } = null!;
    public DbSet<WithdrawalRequest> WithdrawalRequests { get; set; } = null!;

    public CommonContext(DbContextOptions<CommonContext> options) : base(options)
    {
        Database.EnsureCreated();
    }
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        
        modelBuilder.Entity<ConfirmationCode>(entity =>
        {
            entity.ToTable("referal-codes");

            entity.UseCollation("utf8mb4_unicode_ci");
            
            entity.Property(e => e.Address)
                .HasMaxLength(255)
                .HasColumnName("address");

            entity.Property(e => e.Code)
                .HasMaxLength(100)
                .HasColumnName("code");

            entity.Property(e => e.Type)
                .HasMaxLength(10)
                .HasColumnName("type");
            
            entity.HasKey(u => new 
            { 
                u.Type,
                u.Address
            });
        });
        
        modelBuilder.Entity<InstallPrice>(entity =>
        {
            entity.HasKey(e => e.Price)
                .HasName("PRIMARY");

            entity.ToTable("install-price");

            entity.HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.Property(e => e.Price)
                .HasPrecision(10)
                .HasColumnName("price");
            
            entity.HasKey(u => new 
            { 
                u.Price
            });
        });
        modelBuilder.UseCollation("utf8mb4_0900_ai_ci")
                .HasCharSet("utf8mb4");

        modelBuilder.Entity<ActionsCount>(entity =>
        {
            entity.HasKey(e => e.UserId)
                .HasName("PRIMARY");

            entity.ToTable("actions-count");

            entity.HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.Property(e => e.UserId)
                .HasMaxLength(100)
                .HasColumnName("userId");

            entity.Property(e => e.Count).HasColumnName("count");
        });

        modelBuilder.Entity<Article>(entity =>
        {
            entity.ToTable("articles");

            entity.UseCollation("utf8mb4_unicode_ci");

            entity.Property(e => e.Id).HasColumnName("id");

            entity.Property(e => e.Background)
                .HasColumnType("text")
                .HasColumnName("background");

            entity.Property(e => e.Cpu)
                .HasMaxLength(1000)
                .HasColumnName("cpu");

            entity.Property(e => e.Date).HasColumnName("date");

            entity.Property(e => e.DescEn)
                .HasMaxLength(500)
                .HasColumnName("desc_en");

            entity.Property(e => e.DescRu)
                .HasMaxLength(500)
                .HasColumnName("desc_ru");

            entity.Property(e => e.Priority)
                .HasColumnName("priority")
                .HasDefaultValueSql("'0'");

            entity.Property(e => e.TagsEn)
                .HasMaxLength(200)
                .HasColumnName("tags_en");

            entity.Property(e => e.TagsRu)
                .HasMaxLength(200)
                .HasColumnName("tags_ru");

            entity.Property(e => e.TextEn)
                .HasColumnType("mediumtext")
                .HasColumnName("text_en");

            entity.Property(e => e.TextRu)
                .HasColumnType("mediumtext")
                .HasColumnName("text_ru");

            entity.Property(e => e.TitleEn)
                .HasMaxLength(1000)
                .HasColumnName("title_en")
                .UseCollation("utf8_general_ci")
                .HasCharSet("utf8");

            entity.Property(e => e.TitleRu)
                .HasMaxLength(1000)
                .HasColumnName("title_ru")
                .UseCollation("utf8_general_ci")
                .HasCharSet("utf8");

            entity.Property(e => e.TitleTagEn)
                .HasMaxLength(200)
                .HasColumnName("title_tag_en");

            entity.Property(e => e.TitleTagRu)
                .HasMaxLength(200)
                .HasColumnName("title_tag_ru");
        });

        modelBuilder.Entity<Click>(entity =>
        {
            entity.ToTable("clicks");

            entity.UseCollation("utf8mb4_unicode_ci");

            entity.Property(e => e.Date)
                .HasColumnType("datetime")
                .HasColumnName("date");

            entity.Property(e => e.UserId).HasColumnName("userId");
            
            entity.HasOne(c => c.User)
                .WithMany(r => r.Clicks)
                .HasForeignKey(c => c.UserId);
        });

        modelBuilder.Entity<Contact>(entity =>
        {
            entity.ToTable("contacts");

            entity.UseCollation("utf8mb4_unicode_ci");

            entity.Property(e => e.Id).HasColumnName("id");

            entity.Property(e => e.Email)
                .HasMaxLength(100)
                .HasColumnName("email");

            entity.Property(e => e.Name)
                .HasMaxLength(100)
                .HasColumnName("name");

            entity.Property(e => e.PollId).HasColumnName("poll_id");

            entity.Property(e => e.Skype)
                .HasMaxLength(45)
                .HasColumnName("skype");

            entity.Property(e => e.Socials)
                .HasMaxLength(500)
                .HasColumnName("socials");

            entity.Property(e => e.Tg)
                .HasMaxLength(45)
                .HasColumnName("tg");

            entity.Property(e => e.Wv)
                .HasMaxLength(45)
                .HasColumnName("wv");
        });

        modelBuilder.Entity<Install>(entity =>
        {
            entity.HasNoKey();

            entity.ToTable("installs");

            entity.UseCollation("utf8mb4_unicode_ci");

            entity.Property(e => e.Confirmed)
                .HasColumnType("bit(1)")
                .HasColumnName("confirmed")
                .HasDefaultValueSql("b'0'");

            entity.Property(e => e.Date)
                .HasColumnType("datetime")
                .HasColumnName("date");

            entity.Property(e => e.Ip)
                .HasMaxLength(45)
                .HasColumnName("ip");

            entity.Property(e => e.NewUserId)
                .HasMaxLength(45)
                .HasColumnName("newUserId");

            entity.Property(e => e.UserId).HasColumnName("userId");
        });

        modelBuilder.Entity<LastLogRowId>(entity =>
        {
            entity.ToTable("last-log-row-id");

            entity.HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.Property(e => e.Id)
                .HasMaxLength(100)
                .HasColumnName("id");
        });

        modelBuilder.Entity<Poll>(entity =>
        {
            entity.ToTable("polls");

            entity.UseCollation("utf8mb4_unicode_ci");

            entity.Property(e => e.Id).HasColumnName("id");

            entity.Property(e => e.BkCount)
                .HasMaxLength(45)
                .HasColumnName("bk_count");

            entity.Property(e => e.Device)
                .HasMaxLength(45)
                .HasColumnName("device");

            entity.Property(e => e.Frequency)
                .HasMaxLength(45)
                .HasColumnName("frequency");

            entity.Property(e => e.InstrumentList)
                .HasMaxLength(500)
                .HasColumnName("instrument_list");

            entity.Property(e => e.Instruments)
                .HasMaxLength(45)
                .HasColumnName("instruments");

            entity.Property(e => e.Live)
                .HasMaxLength(45)
                .HasColumnName("live");

            entity.Property(e => e.Prematch)
                .HasMaxLength(45)
                .HasColumnName("prematch");

            entity.Property(e => e.Sports)
                .HasMaxLength(45)
                .HasColumnName("sports");
        });

        modelBuilder.Entity<ProDatum>(entity =>
        {
            entity.HasKey(e => e.UserId)
                .HasName("PRIMARY");

            entity.ToTable("pro-data");

            entity.Property(e => e.UserId)
                .ValueGeneratedNever()
                .HasColumnName("userId");

            entity.Property(e => e.ActiveUntil)
                .HasColumnType("datetime")
                .HasColumnName("activeUntil")
                .HasDefaultValueSql("'2020-01-01 14:55:55'");

            entity.Property(e => e.NeedLogout)
                .HasColumnType("bit(1)")
                .HasColumnName("needLogout")
                .HasDefaultValueSql("b'0'");

            entity.Property(e => e.PhoneCheckFails)
                .HasColumnName("phoneCheckFails")
                .HasDefaultValueSql("'0'");

            entity.Property(e => e.TrialAvailable)
                .HasColumnType("bit(1)")
                .HasColumnName("trialAvailable")
                .HasDefaultValueSql("b'1'");
            
            entity.HasOne(d => d.User)
                .WithOne(p => p.ProDatum)
                .HasForeignKey<ProDatum>(d => d.UserId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_pro-data_1");
        });

        modelBuilder.Entity<ProPayment>(entity =>
        {
            entity.ToTable("pro-payments");

            entity.Property(e => e.Id).HasColumnName("id");

            entity.Property(e => e.Date)
                .HasColumnType("datetime")
                .HasColumnName("date");

            entity.Property(e => e.IsTrial)
                .HasMaxLength(45)
                .HasColumnName("isTrial");

            entity.Property(e => e.Length).HasColumnName("length");

            entity.Property(e => e.Name)
                .HasMaxLength(45)
                .HasColumnName("name");

            entity.Property(e => e.Price).HasColumnName("price");

            entity.Property(e => e.PromocodeId).HasColumnName("promocodeId");

            entity.Property(e => e.StartsFrom)
                .HasColumnType("datetime")
                .HasColumnName("startsFrom");

            entity.Property(e => e.UserId).HasColumnName("userId");
        });

        modelBuilder.Entity<ProPeriod>(entity =>
        {
            entity.ToTable("pro-periods");

            entity.UseCollation("utf8mb4_unicode_ci");

            entity.Property(e => e.Id).HasColumnName("id");

            entity.Property(e => e.Discount).HasColumnName("discount");

            entity.Property(e => e.IsTrial)
                .HasColumnType("bit(1)")
                .HasColumnName("isTrial");

            entity.Property(e => e.Length).HasColumnName("length");

            entity.Property(e => e.Name)
                .HasMaxLength(45)
                .HasColumnName("name")
                .UseCollation("utf8mb4_0900_ai_ci");

            entity.Property(e => e.Price).HasColumnName("price");
        });
        
        modelBuilder.Entity<Promocode>(entity =>
        {
            entity.ToTable("promocodes");

            entity.Property(e => e.Id).HasColumnName("id");

            entity.Property(e => e.Code)
                .HasMaxLength(45)
                .HasColumnName("code");

            entity.Property(e => e.Discount).HasColumnName("discount");

            entity.Property(e => e.MaxApplies).HasColumnName("maxApplies");

            entity.Property(e => e.ReferralId).HasColumnName("referralId");

            entity.Property(e => e.ReferralIncome).HasColumnName("referralIncome");

            entity.Property(e => e.TrialLengthIncrease).HasColumnName("trialLengthIncrease");
        });

        modelBuilder.Entity<ReceiptServiceToken>(entity =>
        {
            entity.HasNoKey();

            entity.ToTable("receipt-service-token");

            entity.HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.Property(e => e.Token)
                .HasMaxLength(500)
                .HasColumnName("token")
                .HasDefaultValueSql("'abc'");
        });

        modelBuilder.Entity<Recommendation>(entity =>
        {
            entity.ToTable("recommendations");

            entity.UseCollation("utf8mb4_unicode_ci");

            entity.Property(e => e.Id).HasColumnName("id");

            entity.Property(e => e.Background)
                .HasColumnType("mediumtext")
                .HasColumnName("background");

            entity.Property(e => e.HrefEn)
                .HasMaxLength(255)
                .HasColumnName("href_en");

            entity.Property(e => e.HrefRu)
                .HasMaxLength(255)
                .HasColumnName("href_ru");

            entity.Property(e => e.TitleEn)
                .HasMaxLength(1000)
                .HasColumnName("title_en");

            entity.Property(e => e.TitleRu)
                .HasMaxLength(1000)
                .HasColumnName("title_ru");
        });

        modelBuilder.Entity<Token>(entity =>
        {
            entity.HasNoKey();

            entity.ToTable("tokens");

            entity.UseCollation("utf8mb4_unicode_ci");

            entity.Property(e => e.Token1)
                .HasMaxLength(100)
                .HasColumnName("token");
        });

        modelBuilder.Entity<UsersToCheckPhone>(entity =>
        {
            entity.ToTable("users-to-check-phones");

            entity.Property(e => e.Id).HasColumnName("id");

            entity.Property(e => e.Date).HasColumnName("date");

            entity.Property(e => e.UserIds)
                .HasMaxLength(5000)
                .HasColumnName("userIds")
                .HasDefaultValueSql("''");
        });

        modelBuilder.Entity<WithdrawalRequest>(entity =>
        {
            entity.ToTable("withdrawal-requests");

            entity.HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.Property(e => e.Id).HasColumnName("id");

            entity.Property(e => e.Account)
                .HasMaxLength(45)
                .HasColumnName("account");

            entity.Property(e => e.Approved).HasColumnName("approved");

            entity.Property(e => e.Date)
                .HasColumnType("datetime")
                .HasColumnName("date")
                .HasDefaultValueSql("CURRENT_TIMESTAMP");

            entity.Property(e => e.RejectReason)
                .HasMaxLength(200)
                .HasColumnName("rejectReason")
                .UseCollation("utf8_general_ci")
                .HasCharSet("utf8");

            entity.Property(e => e.Sum)
                .HasPrecision(10)
                .HasColumnName("sum");

            entity.Property(e => e.Type)
                .HasMaxLength(10)
                .HasColumnName("type");

            entity.Property(e => e.UserId).HasColumnName("userId");
        });
    }
}