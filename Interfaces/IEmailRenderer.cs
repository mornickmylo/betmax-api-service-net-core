using BetmaxReferralService.Models;

namespace BetmaxReferralService.Interfaces;

public interface IEmailRenderer
{
    Task<string> Render<T>(T model) where T : EmailModelBase;
}