namespace BetmaxReferralService.Models;

public class UserShort
{
    public string Name { get; set; }
    public string Phone { get; set; }
    public string Email { get; set; }
}