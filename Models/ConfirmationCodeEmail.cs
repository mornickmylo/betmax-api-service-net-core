namespace BetmaxReferralService.Models;

public class ConfirmationCodeEmail : EmailModelBase
{
    public string Text { get; set; }
    public string Reason { get; set; }
}