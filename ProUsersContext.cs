using BetmaxReferralService.Interfaces;
using Microsoft.EntityFrameworkCore;
using BetmaxReferralService.Models;

namespace BetmaxReferralService;

public class ProUsersContext : DbContext, IUserContext<ProUser>
{
    public DbSet<ProUser> Users { get; set; }
    
    public ProUsersContext(DbContextOptions<ProUsersContext> options) : base(options)
    {
        Database.EnsureCreated();
    }

    public ProUser Add(UserShort user)
    {
        var addedEntity = this.Users.Add(new ProUser()
        {
            Phone = user.Phone,
            Email = user.Email,
            Confirmed = true,
            Registered = DateTime.Now,
            Name = user.Name
        });
        this.SaveChanges();
        return addedEntity.Entity;
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<ProUser>(entity =>
        {
            entity.ToTable("pro-users");

            entity.UseCollation("utf8mb4_unicode_ci");

            entity.Property(e => e.Id).HasColumnName("id");

            entity.Property(e => e.Confirmed)
                .HasColumnType("bit(1)")
                .HasColumnName("confirmed");

            entity.Property(e => e.Email)
                .HasMaxLength(255)
                .HasColumnName("email");

            entity.Property(e => e.Name)
                .HasMaxLength(255)
                .HasColumnName("name")
                .UseCollation("utf8mb4_0900_ai_ci");

            entity.Property(e => e.Phone)
                .HasMaxLength(20)
                .HasColumnName("phone");

            entity.Property(e => e.Registered)
                .HasColumnType("datetime")
                .HasColumnName("registered");
        });
        
        modelBuilder.Entity<ProDatum>(entity =>
        {
            entity.HasKey(e => e.UserId)
                .HasName("PRIMARY");

            entity.ToTable("pro-data");

            entity.Property(e => e.UserId)
                .ValueGeneratedNever()
                .HasColumnName("userId");

            entity.Property(e => e.ActiveUntil)
                .HasColumnType("datetime")
                .HasColumnName("activeUntil")
                .HasDefaultValueSql("'2020-01-01 14:55:55'");

            entity.Property(e => e.NeedLogout)
                .HasColumnType("bit(1)")
                .HasColumnName("needLogout")
                .HasDefaultValueSql("b'0'");

            entity.Property(e => e.PhoneCheckFails)
                .HasColumnName("phoneCheckFails")
                .HasDefaultValueSql("'0'");

            entity.Property(e => e.TrialAvailable)
                .HasColumnType("bit(1)")
                .HasColumnName("trialAvailable")
                .HasDefaultValueSql("b'1'");

            entity.HasOne(d => d.User)
                .WithOne(p => p.ProDatum)
                .HasForeignKey<ProDatum>(d => d.UserId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_pro-data_1");
        });
    }
}