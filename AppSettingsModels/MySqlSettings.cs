namespace BetmaxReferralService.AppSettingsModels;

public class MySqlSettings
{
    public string ConnectionString { get; set; }
    public int Major { get; set; }
    public int Minor { get; set; }
    public int Build { get; set; }
}