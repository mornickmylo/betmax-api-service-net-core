using BetmaxReferralService.Interfaces;
using BetmaxReferralService.Models;
using Microsoft.AspNetCore.Mvc;

namespace BetmaxReferralService.Controllers;

[Route("pro")]
public class ProUserController : UserController<ProUser>
{
    private readonly IUserService<ProUser> proUserService;
    public ProUserController(IUserService<ProUser> proUserService) : base(proUserService)
    {
        this.proUserService = proUserService;
    }
}