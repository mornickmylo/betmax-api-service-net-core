namespace BetmaxReferralService.Interfaces;

public interface IUser
{
    int Id { get; set; }
    string Name { get; set; }
    string Phone { get; set; }
    string Email { get; set; }
    DateTime Registered { get; set; }
    bool Confirmed { get; set; }
}