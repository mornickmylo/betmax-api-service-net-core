using BetmaxReferralService.Interfaces;
using Microsoft.AspNetCore.Mvc;
using BetmaxReferralService.Models;
using Microsoft.EntityFrameworkCore;
using ProDatum = BetmaxReferralService.Models.ProDatum;
using ProUser = BetmaxReferralService.Models.ProUser;

namespace BetmaxReferralService.Controllers;

[ApiController]
[Route("tests")]
public class TestsController : ControllerBase
{
    private readonly IUserContext<ProUser> db;
    private readonly IUserContext<Referral> referralsDb;
    private readonly IMailer mailer;
    private readonly IUserService<ProUser> proUserService;
    private readonly IUserService<Referral> referralService;
    private readonly CommonContext commonContext;
    
    public TestsController(IUserContext<ProUser> db, IUserContext<Referral> referralsDb, IMailer mailer, IUserService<ProUser> proUserService, IUserService<Referral> referralService, CommonContext commonContext)
    {
        this.db = db;
        this.referralsDb = referralsDb;
        this.mailer = mailer;
        this.proUserService = proUserService;
        this.referralService = referralService;
        this.commonContext = commonContext;
    }
    
    [HttpGet("pro-data")]
    public ProDatum Index()
    {
        var user = db.Users.Include(u => u.ProDatum).FirstOrDefault();
        user.ProDatum.User.ProDatum = null;
        return user.ProDatum;
    }
    
    [HttpGet("referrals")]
    public IEnumerable<Referral> Referrals()
    {
        return this.referralsDb.Users.AsEnumerable();
    }
    
    [HttpGet("clicks")]
    public IEnumerable<Click> Clicks()
    {
        var referrals = this.referralsDb.Users.Include(u => u.Clicks).Where(r => r.Id == 62);
        var referral = referrals.Single();
        return referral.Clicks;
    }
    
    [HttpGet("mail-test")]
    public async Task MailTest()
    {
        await this.mailer.Send(new ReferralRegisterEmail()
        {
            Subject = "Регистрация в партнерской программе",
            To = "mornickmylo@gmail.com",
            InstallPrice = 101,
            ReferralLink = "https://ya.ru/"
        });
    }
    
    [HttpGet("code-test")]
    public async Task<string?> CodeTest([FromQuery] string type)
    {
        return await this.referralService.SendCode(type, type == "email" ? "mornickmylo@gmail.com" : "79610082142");
    }

    [HttpPost("upsert")]
    public string Upsert(UpsertRequest request)
    {
        return request.NewData == null ? "Insert" : "Update";
    }
}